		var funds = JSON.parse(window.localStorage.getItem('funds'));
		var cart = JSON.parse(window.localStorage.getItem('cart'));
		var image = JSON.parse(window.localStorage.getItem('image'));
		var title = JSON.parse(window.localStorage.getItem('title'));
		var price = JSON.parse(window.localStorage.getItem('price'));
		var stock = JSON.parse(window.localStorage.getItem('stock'));
		var totals = JSON.parse(window.localStorage.getItem('totals'));
		var table;
		var rowCount;
		var row;
		
		function LoadCart() 
		{
		document.getElementById("welcome").innerHTML = "Welcome, User! &nbsp;Funds: P " + funds;
			for(var i = 0; i < cart.length;i++){
				var tname = "table"+i;
				document.getElementById("wrap").innerHTML += "<table id="+tname+"></table>";
				
				table = document.getElementById(tname);
				rowCount = table.rows.length;
				row = table.insertRow(rowCount);
				row.insertCell(0).innerHTML = "<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + image[cart[i]];
				row.insertCell(1).innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + title[cart[i]] + "<br><br>" +
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "P " + price[cart[i]] + "<br><br>" +
				"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + "Easy to understand!";
				document.getElementById("wrap").innerHTML += "<p id="+ tname +"P"+" align=\"right\"><a href=\"#\" id="+ tname +" name="+i+" onclick=\"rem(this)\">Remove from Cart</a>&nbsp;&nbsp;&nbsp;<br>"
				+"__________________________________________________________________________________________________________________________________________________________________</p>";
				
			}
		}
		function rem(i)
		{
			cart.pop(i);
			totals = totals - price[i.name];
			window.localStorage.setItem('totals',JSON.stringify(totals));
			var trem = document.getElementById(i.id);
			trem.parentNode.removeChild(trem);
			var prem = document.getElementById(i.id+"P");
			prem.parentNode.removeChild(prem);
			var hrem = document.getElementById(i.id+"H");
			prem.parentNode.removeChild(hrem);
			
			
			
		}
		function purchase()
		{
			if(funds>totals){
				window.localStorage.setItem('cart',JSON.stringify(cart));
				window.location.href = "receipt.html";
			}
			else{
				alert("Insufficient funds!")
			}
			
		}
		
		function back()
		{
			window.localStorage.setItem('funds',JSON.stringify(funds));
			window.localStorage.setItem('cart',JSON.stringify(cart));
			window.localStorage.setItem('stock',JSON.stringify(stock));
			window.location.href = "body.html";
		}